"""
@file main_L2.py

@package BlinkingLEDs

@brief This is the main file for lab 2 - Blinking LEDs

@details This file simply calls two tasks to run simultanously through my finite
state machine that is titled SC_blink.py

@date Oct 14 2020

@author: ben_p
"""

## Call Source Code for the Lab
import SC_blink

#Define Tasks to Run
task1 = SC_blink.TaskBlink(1,'Blink','Virtual') #1 second period blinking on a virtual LED
task2 = SC_blink.TaskBlink(10,'Sine','Physical') #10 second period iluminating in a sinusoidal pattern on the physical LED
    
# Run the tasks in sequence over and over again
for N in range(100000): # effectively while(True):
    task1.run()
    task2.run()