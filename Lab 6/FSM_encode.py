"""
@file FSM_encode.py

@package Lab6

@brief Source Code for encoder objects

@details This source code provides the tools necessary to read
data from a motor's encoder. The code utilizes pyb and PWM
functions that are built into Python. There is also a function to relay
time responses into the code to allow for easy recording and graphing as
we have seen in labs 4 and 6. The recording resolution is set to 0.025 seconds
but can be easily adjusted as desired. The encoder has been new and improved and sends velocity in RPM. \n
Source Code: https://bitbucket.org/BENNYB0P/mechatronics/src/master/Lab%206/FSM_encode.py

@date Oct 21 2020

@author: Ben Presley
"""

import pyb
from pyb import UART
myuart = UART(2)

class EncoderDriver:
    '''
    @brief Communicates with hardware to get encoder data 
    @details This driver is key for accessing data from the motor's encoder
    which will be key for many projects in this course/profession.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
    ## Constant defining State 1 - Read Timer
    S1_UPDATE           = 1    
    
    ## Constant defining State 2 - Interpret UPDATE
    S2_getDelta         = 2    
    
    ## Constant defining State 3 - Return Position
    S3_getPos           = 3
    
    ## Constant defining State 4 - Turn LED off
    S4_setPos           = 4
    
    
    def __init__(self, pin1, pin2, enc_timer, ch1, ch2, gear_ratio):
        '''
        @brief Established the parameters of the system
        '''
        ## Set state to zero
        self.state = self.S0_INIT
        
        ##Define timer and pins
        self.pin1 = pin1
        self.pin2 = pin2
        self.enc_timer = enc_timer
        self.ch1 = ch1
        self.ch2 = ch2
        
        ## The timestamp for the first iteration
        self.start_time = pyb.millis()
        
        ## Define Gear Ratio to get deg/tick
        self.gear_ratio = gear_ratio
        
        self.curr_pos = 0 #original location
        self.Delta = 0 #orininal delta
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + 200
        
        ## Set recording data state to False
        self.recording = 0
        
        ## Recording Resolution
        self.Resolution = 25 #[ms], please do not go below 50 ms.
        
        # Define Delta Interval
        self.deltaInterval = 10
        
    def run(self):
        '''
        @brief Tasks run within state-machine and allow for multitasking
        @details The encoder must account for overflow and underflow. The
        program takes advantage of the timer function for pyb. The mode was 
        set to ENC_AB for easiest implementation of clockwise and
        counterclockwise movements. Note that the position is still just the
        ticks of the timer. The ticks have not been calibrated to degrees yet.
        The code also has a recording function built in!
        '''
        # State time conditions
        self.curr_time = pyb.millis()
        
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code - set Up Timer!
                self.tim = pyb.Timer(self.enc_timer)
                self.tim.init(prescaler=0,period=0xFFFF)
                self.tim.channel(self.ch1,pin=self.pin1,mode=pyb.Timer.ENC_AB)
                self.tim.channel(self.ch2,pin=self.pin2,mode=pyb.Timer.ENC_AB)
                self.oldTick = self.tim.counter() #original location
                self.curr_speed = 0 #must define now
                self.transitionTo(self.S1_UPDATE)

                
            elif(self.state == self.S1_UPDATE):
                # Run State 1 Code - Get info from Timer
                self.newTick = self.tim.counter() #simply snag data

                #Compute good or bad delta
                self.Delta = self.newTick - self.oldTick
                if(self.Delta == 0):
                    self.newDelta = 0
                elif(self.Delta < (-0xFFFF/2)): #overflow case
                    self.newDelta = (0xFFFF - self.oldTick) + self.newTick
                elif(self.Delta > (0xFFFF/2)): # underflow case
                    self.newDelta = -((0xFFFF - self.newTick) + self.oldTick)
                else: #normal case
                    self.newDelta = self.Delta #ticks
                
                #Define new position
                self.curr_pos = self.curr_pos + self.newDelta #ticks
                
                #Define new speed
                self.curr_speed = self.newDelta*self.gear_ratio*1000/self.deltaInterval/6 #rpm
                
                ## RECORDING DATA ##
                if(self.recording == 1):
                    if(self.curr_time - self.Resolution >= self.record_tlast): #record at resolution
                        print(str((self.curr_time - self.record_tinit)/1000) + ', '+ str(self.curr_speed))
                        self.record_tlast = self.curr_time
                        
                    if((self.curr_time - self.record_tinit) > 10000): #after ten seconds of recording
                        self.recording = 0 #set recording to false
                        print('Recording Finished') #state the recording has stopped.
                        
                self.oldTick = self.newTick #redefine what the old tick is now
            self.next_time = self.curr_time + self.deltaInterval
            
    def getPos(self):
        '''
        @brief return current position
        '''
        return str('Current Postion: ' +str(self.curr_pos*self.gear_ratio))
    
    def setPos(self):
        '''@brief sets position to zero'''
        self.curr_pos = 0
        return str('Position reset to zero')
        
    def showDelta(self):
        '''
        @brief return current delta
        '''
        return str('Current Delta: '+str(self.Delta))
        
        
    def recordData(self):
        ''' @brief returns a data list of time vs position for 10 seconds'''
        self.recording = 1
        self.record_tinit = self.curr_time
        self.record_tlast = self.curr_time
        self.record_datainit = self.curr_pos
        print('Recording Started')


    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState
        

        