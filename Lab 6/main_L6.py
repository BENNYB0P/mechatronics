"""
@file main_L6.py

@package Lab6

@brief Source Code for encoder objects

@details This main code manages the function and scripts for package \ref Lab6. 
The scripts are set up as finite state machines to allow for multitasking. The Nucleos reset button will be useful when you start up the
user interface code: \ref Spyder_UserInterface.py \n
Source Code: https://bitbucket.org/BENNYB0P/mechatronics/src/master/Lab%206/main_L6.py

@date November 25th, 2020

@author: Ben Presley
"""

## Import necessary published
import pyb #useful library
from pyb import UART #for communication with Spyder
import os #useful library


## Import my source code files (that must already be put on Nucleo)
import FSM_encode
import moset
import SC_closedloop

## Define Serial Port for Handshake
myuart = UART(2)

## Send go ahead to PC
print('GoPC') #This string will prompt Spyder to move forward
pyb.delay(1000) #give Spyder a second to transfer into operation state

#----------------------------------------------------------------------------#
  #This code runs when LED begins to shine - make sure Spyder is connected#

## Define Tasks - these run on the Nucleo!
pyb.delay(25)
myEncoder1 = FSM_encode.EncoderDriver(pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, 4, 1, 2, 360/4000) #established encoder 1
print('Encoder 1 Initialized')

# pyb.delay(25)
# myEncoder2 = FSM_encode.EncoderDriver(pyb.Pin.cpu.C6, pyb.Pin.cpu.C7, 8, 1, 2, 360/4000) #established encoder 1
# print('Encoder 2 Initialized')

pyb.delay(25)
myMotor1 = moset.MotorDriver(pyb.Pin.cpu.A15, pyb.Pin.cpu.B4, pyb.Pin.cpu.B5, 3, 1, 2)
print('Motor 1 Initialized')

# pyb.delay(25)
# myMotor2 = MotorDriver(None, pyb.Pin.cpu.B0, pyb.Pin.cpu.B1, 3, 3, 4)
# print('Motor 1 Initialized')

pyb.delay(25)
myController1 = SC_closedloop.ClosedLoop(myMotor1, myEncoder1, myuart)
print('Controller 1 Activated')


##----------------------Run Finite State Machine Tasks----------------------##
while True:
    #check for any special requests
    if myuart.any() !=0:
        cmd = myuart.readchar()
        if cmd == 'z' or cmd == 122: #if z is pressed
            print(myEncoder1.setPos())
        elif cmd == 'p' or cmd == 112: #if p is pressed
            print(myEncoder1.getPos())
        elif cmd == 'd' or cmd == 100: #if d is pressed
            print(myEncoder1.showDelta())
        elif cmd == 'g' or cmd == 103: #if g is pressed
            myEncoder1.recordData()
        elif cmd == 's' or cmd == 115:
            myEncoder1.recording = 0
            print('Recording Stopped')
        elif cmd == 'm' or cmd == 109:
            myMotor1.Forward(50)
        elif cmd == 'k' or cmd == 107:
            pyb.delay(50) #make sure nothing old is read in the mean time
            myController1.setKp()
        elif cmd == 'd' or cmd == 100:
            pyb.delay(50)
            myController1.setDesired()
        elif cmd == 'x' or cmd == 120:
            myController1.running = 1
            myEncoder1.recordData()
        else:
            print('Nucleo has no function for entry: ', cmd)


    #update tasks as usual
    myEncoder1.run()
    myController1.Update(myEncoder1.curr_speed)
    # myEncoder2.run()
