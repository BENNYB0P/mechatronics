# -*- coding: utf-8 -*-
"""
@file moset.py

@package Lab6

@brief File for setting up motor pins to execute quickly

@details This file is how I quickly set up my pins for my motors which are attached to a breakout board
equipped with an H bridge from Texas Instruments. There are 5 pins to set up in PWM that send high() or low()
signals. The H bridge then can send a sequence of DC motor functions to each motor. These functions include: \n
Motor Coast \n Motor Brake \n Motor Forward \n Motor Reverse \n\n The H bridge can control two motors, so we will set
up the system to do both!\n
Source Code Link: https://bitbucket.org/BENNYB0P/mechatronics/src/master/Lab%206/moset.py


@date Nov 16th, 2020


"""


import pyb

class MotorDriver:
    ''' @brief Driver for DC motor
    @details This file is how I quickly set up my pins for my motors which are attached to a breakout board
    equipped with an H bridge from Texas Instruments. There are 5 pins to set up in PWM that send high() or low()
    signals. The H bridge then can send a sequence of DC motor functions to each motor. These functions include: \n
    Motor Coast \n Motor Brake \n Motor Forward \n Motor Reverse \n\n The H bridge can control two motors, so we will set
    up the system to do both!
    ''' 
    
    #Set up pins - below is the scheme for lab 0x06
        #nSLEEP = PA_15 = A15
        #IN1 = PB_4 = B4
        #IN2 = PB_5 = B5
        #IN3 = PB_0 = B0
        #IN4 = PB_1 = B1
        
    def __init__(self, nSLEEP_pin, IN1_pin, IN2_pin, timer, ch1, ch2):
        #initalize pin objects
        self.IN1_pin = pyb.Pin (IN1_pin)
        self.IN2_pin = pyb.Pin (IN2_pin)
        
        #Define Channels
        self.ch1 = ch1
        self.ch2 = ch2
        
        #Initialize Ultra-precise Timer
        self.myTimer = pyb.Timer(timer, freq = 1000)
        
        #Assign nSLEEP
        if (nSLEEP_pin == None):
            print('H-Bridge already initialized')
        else:
            self.nSLEEP_pin = pyb.Pin (nSLEEP_pin)
            self.nTimer = pyb.Timer(2, freq = 1000)
            self.nSLEEPchannel = self.nTimer.channel(1,pyb.Timer.PWM, pin=self.nSLEEP_pin, pulse_width_percent = 100) #set nSLEEP pin high
        
        self.Coast()
    
    def AutoDuty(self, Duty):
        if(Duty == 0):
            self.Coast()
            
        elif(Duty<0):
            self.Reverse(Duty)
            
        elif(Duty>0):
            self.Forward(Duty)
            
    def Forward(self, Duty):
        self.IN1 = self.myTimer.channel(self.ch1,pyb.Timer.PWM, pin=self.IN1_pin, pulse_width_percent = Duty)
        self.IN2 = self.myTimer.channel(self.ch2,pyb.Timer.PWM, pin=self.IN2_pin, pulse_width_percent = 0)
        # print('Motor Spinning in Forward motion')
        
    def Reverse(self, Duty):
        self.IN1 = self.myTimer.channel(self.ch1,pyb.Timer.PWM, pin=self.IN1_pin, pulse_width_percent = 0)
        self.IN2 = self.myTimer.channel(self.ch2,pyb.Timer.PWM, pin=self.IN2_pin, pulse_width_percent = -(Duty))
        # print('Motor Spinning in Reverse motion')
    
    def Coast(self):
        self.IN1 = self.myTimer.channel(self.ch1,pyb.Timer.PWM, pin=self.IN1_pin, pulse_width_percent = 0)
        self.IN2 = self.myTimer.channel(self.ch2,pyb.Timer.PWM, pin=self.IN2_pin, pulse_width_percent = 0)
        # print('Motor Coasting')
        
    def Brake(self):
        self.IN1 = self.myTimer.channel(self.ch1,pyb.Timer.PWM, pin=self.IN1_pin, pulse_width_percent = 100)
        self.IN2 = self.myTimer.channel(self.ch2,pyb.Timer.PWM, pin=self.IN2_pin, pulse_width_percent = 100)
        # print('Motor applied Brakes')
        
    def Disable(self):
        self.nSLEEPchannel = self.nTimer.channel(1,pyb.Timer.PWM, pin=self.nSLEEP_pin, pulse_width_percent = 0) #set nSLEEP pin low
        # print('Motor has been put to sleep.\n Command "Motor.Wake" to resume.')
    
    def Enable(self):
        self.nSLEEPchannel = self.nTimer.channel(1,pyb.Timer.PWM, pin=self.nSLEEP_pin, pulse_width_percent = 100) #set nSLEEP pin high
        # print('Motor has been awoken.')
        
