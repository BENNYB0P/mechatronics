# -*- coding: utf-8 -*-
"""
@file SC_closedloop.py

@package Lab6

@brief Closed Loop Proportional Controller

@details This source code is meant to be robust and work for multiple uses. The desired variable can
be set, and the proportional gain can also be set. This is very useful. The code can also set limits,
we do not want to oversaturate any components! Just make sure to keep
track of units throughout! This code was apart of \ref Lab6
\n Source Code: https://bitbucket.org/BENNYB0P/mechatronics/src/master/Lab%206/SC_closedloop.py
\n\n
The code was tested with a DC motor trying to hit a speed target of 1000 rpm, and the following plots show the Kp value being narrowed down to desirable results!
\n
\image html "lab6_test1.png"
\n
\image html "lab6_test2.png"
\n
\image html "lab6_test3.png"
\n
\image html "lab6_test4.png"
\n
\image html "lab6_test5.png"

@author: Ben Presley

@date November 25, 2020

@copyright Consent of Ben Presley before use.
"""
import pyb

class ClosedLoop:
    ''' 
    @brief basic proportional controller which functions can be called
    @details This class was developed for lab 6, where the encoder will provide sensory
    information on teh state of the motor. This system is set up to work for any
    circumstance of control -- whether it be control of position or velocity.
    
    '''
    
    def __init__(self, myMotor, myEncoder, myuart):
        self.myEncoder = myEncoder
        self.myMotor = myMotor
        self.myuart = myuart
        self.PWM_max = 100
        self.PWM_min = 0 #we do NOT want negative values (no backdrive)
        
        self.running = 0
        self.Desired = 0
        
        self.setKp() #call the setKp function
        
        self.setDesired() #call setDesired function
    
    def Update(self, actual):
        if self.running == 1:
            self.Error = self.Desired - actual
            self.proposedDuty = self.Error*self.Kp
            self.duty = self.clamp(self.proposedDuty)
            self.myMotor.AutoDuty(self.duty)
        else:
            self.duty = 0
        
    
    def getKp(self):
        while True: #wait for an input from spyderspace
            try:
                if self.myuart.any() != 0:
                    self.Kpcmd = self.myuart.read()
                    self.Kp_clean0= self.Kpcmd.decode('ascii')
                    self.Kp_clean1 = self.Kp_clean0.strip('Kp: ')
                    self.Kp = float(self.Kp_clean1)
                    break
            except ValueError:
                pass
        print('Nucleo set Kp to: ',self.Kp)
    
    def setKp(self):
        print('\nAwaiting Kp value from user') #prompts spyderspace
        pyb.delay(100) #make sure nothing old is read in the mean time
        return self.getKp()
    
    def getDesired(self):
        while True: #wait for an input from spyderspace
            try:
                if self.myuart.any() != 0:
                    self.Descmd = self.myuart.read()
                    self.Des_clean0 = self.Descmd.decode('ascii')
                    self.Des_clean1 = self.Des_clean0.strip('Des: ')
                    self.Desired = float(self.Des_clean1)
                    break
            except ValueError:
                pass
        print('Nucleo set Desired to: ',self.Desired)
    
    def setDesired(self):
        print('\nAwaiting Desired value from user') #prompts spyderspace
        pyb.delay(100) #make sure nothing old is read in the mean time
        return self.getDesired()
        
    def clamp(self, n):
        '''@brief Limits values based on min and max values'''
        
        if n < self.PWM_min:
            return self.PWM_min
        elif n > self.PWM_max:
            return self.PWM_max
        else:
            return n
        
        