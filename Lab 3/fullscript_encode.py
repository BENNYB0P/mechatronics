"""
@file fullscript_encode.py

@package EncoderDriver

@breif Contains all contents for Lab 3

@details This source code provides the tools and code necessary to read
data from a motor's encoder. The code utilizes pyb and PWM
functions that are built into Python. The code contains all the code for lab 3
to prevent a confusing publishing process. The main file and user inputs are
thus included. \n
Source Code: 

@date Oct 21 2020

@author: Ben Presley
"""

import pyb
from pyb import UART

print('Running')

class EncoderDriver:
    '''
    @brief Communicates with hardware to get encoder data 
    @details This driver is key for accessing data from the motor's encoder
    which will be key for many projects in this course/profession.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
    ## Constant defining State 1 - Read Timer
    S1_UPDATE           = 1    
    
    ## Constant defining State 2 - Interpret UPDATE
    S2_getDelta         = 2    
    
    ## Constant defining State 3 - Return Position
    S3_getPos           = 3
    
    ## Constant defining State 4 - Turn LED off
    S4_setPos           = 4
    
    
    def __init__(self):
        '''
        @brief Established the parameters of the system
        '''
        ## Set state to zero
        self.state = self.S0_INIT
        
        ## The timestamp for the first iteration
        self.start_time = pyb.millis()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + 2000
        
    def run(self):
        '''
        @brief Tasks run within state-machine and allow for multitasking
        @details The encoder must account for overflow and underflow. The
        program takes advantage of the timer function for pyb. The mode was 
        set to ENC_AB for easiest implementation of clockwise and
        counterclockwise movements. Note that the position is still just the
        ticks of the timer. The ticks have not been calibrated to degrees yet.
        '''
        # State time conditions
        self.curr_time = pyb.millis()
        
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code - set Up Timer!
                self.tim = pyb.Timer(4)
                self.tim.init(prescaler=1,period=0xFFFF)
                self.tim.channel(1,pin=pyb.Pin.cpu.B6,mode=pyb.Timer.ENC_AB)
                self.tim.channel(2,pin=pyb.Pin.cpu.B7,mode=pyb.Timer.ENC_AB)
                self.oldTick = self.tim.counter() #original location
                self.curr_pos = 0 #original location
                self.transitionTo(self.S1_UPDATE)
                
            elif(self.state == self.S1_UPDATE):
                # Run State 1 Code - Get info from Timer
                self.newTick = self.tim.counter() #simply snag data
                self.transitionTo(self.S2_getDelta)
                
            
            elif(self.state == self.S2_getDelta): #interpret data
                # Run State 2 Code - Interpert and add to Position
                self.Delta = self.newTick - self.oldTick
                
                if(self.Delta == 0):
                    self.transitionTo(self.S1_UPDATE)
                elif(self.Delta < (-0xFFFF/2)): #overflow case
                    self.curr_pos = self.curr_pos + (0xFFFF - self.oldTick) + self.newTick
                    self.transitionTo(self.S1_UPDATE)
                elif(self.Delta > (0xFFFF/2)): # underflow case
                    self.curr_pos = self.curr_pos - ((0xFFFF - self.newTick) + self.oldTick)
                    self.transitionTo(self.S1_UPDATE)
                else: #normal case
                    self.curr_pos = self.curr_pos + self.Delta
                self.oldTick = self.newTick #redefine what the old tick is now
            
    def getPos(self):
        '''
        @brief return current position
        '''
        print('Current Postion: ',self.curr_pos)
    
    def setPos(self):
        '''@brief sets position to zero'''
        self.curr_pos = 0
        print('Position reset to zero')
        self.getPos() #print current position to make sure!
        
    def showDelta(self):
        '''
        @brief return current delta -- will be good for vel. later
        '''
        print('Current Delta: ',self.Delta)
        
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
        
# ---------------------------------------------------------------------------#
        ## This section of code could be placed in a new script ##
                
           
class UIrequest:
    '''
    @brief Communicates with user to get desired functions 
    @details Let's the user enter which paramteres they want to view.
    '''
    
    ## Initialization state
    S0_INIT             = 0
    
    ## Waitign for character state
    S1_WAIT_FOR_CHAR    = 1
    
    ## Waiting for response state
    S2_CALL_FUNCTION    = 2
    
    
    def __init__(self):
        '''
        @brief Established the parameters of the input system
        '''
        ## Set state to zero
        self.state = self.S0_INIT
        
        ## Read inputs every 10 ms
        self.Rate = 10
        
        ## The timestamp for the first iteration
        self.start_time = pyb.millis()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.Rate
        
        ## Serial port
        self.ser = UART(2)
        
        
    def run(self):
        '''
        @brief Tasks run within state-machine and allow for multitasking
        @details The user interface is key. We will take advantage of UART
        which is a function inside of pyb. The state-machine will collect 
        the inputs to the system and then designate tasks back to the
        encoder class
        '''
        # State time conditions
        self.curr_time = pyb.millis()
        
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_CHAR)
            
            elif(self.state == self.S1_WAIT_FOR_CHAR):
                # Run State 1 Code
                if self.ser.any():
                    self.cmd = self.ser.readchar()
                    self.transitionTo(self.S2_CALL_FUNCTION)

            elif(self.state == self.S2_CALL_FUNCTION):
                #Run State 2 Code
                if self.cmd == 90 or self.cmd == 122: #if Z/z is pressed
                    Encoder1.setPos()
                if self.cmd == 80 or self.cmd == 112: #if P/p is pressed
                    Encoder1.getPos()
                if self.cmd == 68 or self.cmd == 100: #if P/p is pressed
                    Encoder1.showDelta()
                self.transitionTo(self.S1_WAIT_FOR_CHAR)

    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState    



#----------------------------------------------------------------------------#
        ## This section of code could be placed in a new script ##

Encoder1 = EncoderDriver()
UI_1 = UIrequest()
    
# Run the tasks in sequence over and over again
for N in range(100000): # effectively while(True):
    Encoder1.run()
    UI_1.run()