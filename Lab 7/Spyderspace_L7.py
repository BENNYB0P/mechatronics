"""
@file Spyderspace_2.py

@package Lab7

@brief Spyder script to interact real time with Nucleo

@details This scripts incorporates the serial functionality to communicate
with the Nucleo. It is a new and improved script that gives Spyder the ability to communicate
with the Nucluo in real time. The Nucleo is tasked to run the \ref main_L6.py script -- which
manages the Sinusoidal LED pattern and keeps track of the encoder, while also
running the controller design tool and trackers. The encoderuses the
recording function which allows the data to be plotted to interpret the changes of
the controller gain. 
The plotting functionality is very nice and robust for this application.  \n
Source Code: https://bitbucket.org/BENNYB0P/mechatronics/src/master/Lab%207/Spyderspace.py

\n \n
The spyderspace communicated with the Nucleo and produced the following syncronized
movements. The orange line is the Spyderspace's request, and the blue line is the actual.

\image html lab7_test_J484478.png

\n The J value was 484,000 for this test. With Kp = 0.12 and Ki = 0.012

@date Dec 4 2020

@author: Ben Presley
"""

import serial #for communication with Nucleo
import keyboard #for Spyder detecting keyboard presses.
import time #timekeeping
import matplotlib.pyplot as plt #for plotting
import sys #for exit function onlyg

ser = serial.Serial(port='COM3',baudrate=115273,timeout=.1) #comms with Nucleo


#----------------------------Load Data---------------------------------------#
## Loading data function for position/velocity tracking in Lab 7

# Define variables as lists -- the variables can be changed if we are not
# talking about time, velocity or position
ref_time = []
ref_velocity = []
ref_position = []
scalar = 10

# Open the filename file and save as the reference
ref = open('reference.csv')

# Unpackage the data
while True:
    # read the data of a line
    line = ref.readline()
    # check if the csv has been completetly read
    if line == '':
        break
    # seperate csv by commas and append to list as a float
    else:
        (ti, vi, xi) = line.strip().split(',');
        ref_time.append(float(ti))
        ref_velocity.append(float(vi)*scalar)
        ref_position.append(float(xi)*scalar)
ref.close() 
#----------------------loading has been completed!---------------------------#
    
    
## My FSM Spyder user interface class
class UIrequest:
    '''
    @brief User Interface for robust transfer of Inputs and Output 
    @details This code allows the user to input keystrokes to Spyder which sends commands
    to the Nucleo. The code will also respond to outputs from the Nucleo and 
    harvest/store data when necessary. For lab 0x04 there is also a plotting
    functionality incorporated into the code.
    
    '''
    
    ## Initialization state
    S0_INIT           = 0
    
    ## Communication state - runs forever
    S1_COMMUNICATE    = 1
    
    def __init__(self):
        '''
        @brief Established the parameters of the input system
        '''
        ## Set state to zero
        self.state = self.S0_INIT
        
        ## Run communication sequence every 25 ms
        self.Rate = 30
        
        ## The timestamp for the first iteration
        self.start_time = time.time()*1000
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.Rate
        
        #Set default target encoder/motor pair to 1
        self.Target = '1'
        
        #Make sure recording value is False
        self.Recording = False
        self.time_plot = [0, 0]
        self.rpm_plot = [0, 0]
        self.deg_plot = [0, 0]
        self.AlreadyRecorded = False
        
        #Define state of Movement Syncronization
        self.Syncing = False
        
        #Define old out and blanks
        self.oldout = 'GoPC'
        self.blank = ''
        
        ## Confirm code is running
        print('Spyderspace Engaged. Waiting for connection to Nucleo\n\nPress reset button on Nucleo to start...\n')
    
    def run(self):
        # State time conditions
        self.curr_time = time.time()*1000
        
        
        if self.curr_time > self.next_time:
            
            #State 0 Code - runs initially
            if(self.state == self.S0_INIT):
                self.checkgo = self.readLine()
                if(self.checkgo=='GoPC'):
                    print('Successful connection to Nucleo!')
                    print('\nAvailable functions:')
                    print('  p = get position')
                    print('  z = reset position to zero')
                    print('  d = get current speed')
                    print('  g = get data for 10 seconds')
                    print('  s = stop getting data')
                    print('  k = edit proportional gain')
                    print('  i = edit integral gain')
                    print('  x = execute and record step function')
                    print('  f = follow movement profile')
                    print('  esc = exit and render results')
                    print('\nPress tab to switch target encoder/motor pair')
                    print('Default Target: ',self.Target,'\n')
                    
                    self.transitionTo(self.S1_COMMUNICATE)

                
            #State 1 Code - runs forever repeating
            elif(self.state == self.S1_COMMUNICATE):
                
                #Send any inputs to Nucleo if critical events are false
                if(self.Recording == False and self.Syncing == False):
                    
                    if(keyboard.is_pressed('g')): #get data for 10 seconds
                       if(self.AlreadyRecorded == False):
                           self.sendLine('g')
                       else:
                           print('You have already recorded data')
                    
                    elif(keyboard.is_pressed('p')): #get current position
                        self.sendLine('p,'+self.Target+',0,0') 
                    
                    elif(keyboard.is_pressed('z')): #reset position to zero
                        self.sendLine('z,'+self.Target+',0,0')
                    
                    elif(keyboard.is_pressed('d')): #get current speed
                        self.sendLine('d,'+self.Target+',0,0')
                    
                    elif(keyboard.is_pressed('f')): #follow movement profile
                        self.sendLine('f,'+self.Target+',0,0')
                        self.Syncing = True
                        time.sleep(.2)
                        self.ProfileStartTime = self.curr_time

                    elif(keyboard.is_pressed('k')): #set proportional gain
                        self.inputKp = input('Enter Proportional gain, Kp: ')
                        self.sendLine('k,'+self.Target+','+self.inputKp+',0')
                        
                    elif(keyboard.is_pressed('i')): #set proportional gain
                        self.inputKi = input('Enter Integral gain, Ki: ')
                        self.sendLine('i,'+self.Target+','+self.inputKi+',0')     
                        
                    elif(keyboard.is_pressed('m')): #manually start motor
                        self.inputPWM = input('Enter PWM setting: ')
                        self.sendLine('m,'+self.Target+','+self.inputPWM+',0')
                        
                    elif(keyboard.is_pressed('x')): #execute and record a step function
                        if(self.AlreadyRecorded == False):
                            self.inputSpeed = input('Enter step speed: ')
                            self.sendLine('x,'+self.Target+','+self.inputSpeed+',0')
                        else:
                           print('You have already recorded data')
                    
                    elif(keyboard.is_pressed('tab')): #Toggle motor/encoder pair
                        if self.Target == '1':
                            self.Target = '2'
                        else:
                            self.Target = '1'
                        print('New Target: ',self.Target)
                           
                    elif(keyboard.is_pressed('esc')):
                        ser.close()
                        sys.exit()
                    
                if(keyboard.is_pressed('s')):
                    if(self.AlreadyRecorded == False):
                        self.sendLine('s')

                # Toggle pair
                if(keyboard.is_pressed('tab')):
                    if self.Target == '1':
                        self.Target = '2'
                    else:
                        self.Target = '1'
                    print('New Target: ',self.Target)
                    
                # Send movement profile as time goes onwards
                if self.Syncing == True:    
                    self.IndexTime = self.curr_time - self.ProfileStartTime
                    if(self.IndexTime < 15000):
                        self.sendLine('~,'+self.Target+','+str(ref_velocity[int(self.IndexTime)])+','+str(ref_position[int(self.IndexTime)]))
                    else:
                        self.Syncing = False
                        time.sleep(.1)
                        self.sendLine('f,'+self.Target+',0,0')

                # Recieve any outputs from Nucleo
                self.out = self.readLine()
                if(self.out != self.oldout and self.out != self.blank):
                    print(self.out)
                    
                    if(self.out == 'Recording Finished' or self.out == 'Recording Stopped'):
                        self.Recording = False
                        self.AlreadyRecorded = True
                        self.PlotResults(self.time_plot, self.rpm_plot, self.deg_plot)
                        print('To exit and render results, press "esc"')
                        
                    if(self.Recording == True):
                        self.cleanstring = self.out
                        self.line_list = self.cleanstring.strip().split(', ')
                        try:
                            self.time_plot.append(float(self.line_list[0]))
                            self.rpm_plot.append(float(self.line_list[1]))
                            self.deg_plot.append(float(self.line_list[2]))
                        except ValueError:
                            pass
                        
                    if(self.out == 'Recording Started'):
                        self.Recording = True
                        
                    if(self.out == 'Tracking Algorithm Ready'):
                        self.Syncing = True
                    
                    if(self.out == 'Tracking Algorithm Finished'):
                        self.Syncing = False
                    
                    if(self.out == 'Awaiting Kp value from user'):
                        self.sendKp = input('Enter a value for Kp: ')
                        self.Kpcmd = str(self.sendKp)
                        self.sendLine(str('Kp: '+self.Kpcmd))
                        
                self.oldout = self.out # define old data so we dont overprint it
            
            self.next_time = self.curr_time+self.Rate # run again at rate
            
        else:
            pass
                
    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        '''
        self.state = newState
        
    def sendLine(self, string):
        ''' @brief Sends a line of code through the serial bus
        @details I used ascii encoding because it seems to behave nicely with
        the back ends use of UART -- and we were only dealing with sending one
        character'''
        ser.flush()
        self.sendit = str(string).encode('ascii')
        ser.write(self.sendit)
            
        
    def readLine(self):
        ''' @brief Recieves the most recent console line in the Nucleo
        @details I used -utf-8 decoding because it seemed to behave nicely with
        the front end (Spyder) and transfer large strings'''
        self.rawread = str(ser.readline().decode('-utf-8'))
        self.cleanread = self.rawread.strip('\r\n')
        return self.cleanread

    def PlotResults(self, xlist, ylist1, ylist2):
        ''' @brief plots given data series.'''
        x_vals = xlist
        y_vals1 = ylist1
        y_vals2 = ylist2
    
        plt.figure(1)
        plt.subplot(2,1,1)
        plt.plot(x_vals, y_vals1)

        plt.plot(ref_time, ref_velocity)

        plt.ylabel('Velocity, [rpm]')

        plt.subplot(2,1,2)
        plt.plot(x_vals, y_vals2)

        plt.plot(ref_time, ref_position)

        plt.ylabel('Position, [deg]')
        plt.xlabel('Time, [s]')
        f
        
##--------------------------------------------------------------------------##

# Run the script from Spyder
myInterface = UIrequest()


for N in range(100000000): # effectively while(True):
    myInterface.run()
    
ser.close() #close serial connection properly
