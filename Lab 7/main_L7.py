"""
@file main_L7.py

@package Lab7

@breif main script for \ref Lab7

@details This main code manages the function and scripts for package \ref Lab7. 
The scripts are set up as finite state machines to allow for multitasking. The Nucleos reset button will be useful when you start up the
user interface code: \ref Spyder_UserInterface.py \n
Source Code: https://bitbucket.org/BENNYB0P/mechatronics/src/master/Lab%207/main.py

@date Dec 04 2020

@author: Ben Presley
"""

## Import necessary published
import pyb #useful library
from pyb import UART #for communication with Spyder


## Import my source code files (that must already be put on Nucleo)
import FSM_encode
import moset
import SC_closedloop

## Define Serial Port for Handshake
myuart = UART(2)

## Send go ahead to PC
print('GoPC') #This string will prompt Spyder to move forward
pyb.delay(500) #give Spyder a second to transfer into operation state
                

#-----------------------------Input Setup------------------------------------#
lastInput = '-'
target = 0  #target for data (which encoder/motor pair)
data1 = 0.0 #channel 1 for data input
data2 = 0.0 #channel 2 for data input
count_blanks = 0
Syncing = False
DesiredSpeed = 0
DesiredPosition = 0


#----------------------------------------------------------------------------#
  #This code runs when LED begins to shine - make sure Spyder is connected#

## Define Tasks - these run on the Nucleo!
pyb.delay(25)
myEncoder1 = FSM_encode.EncoderDriver(pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, 4, 1, 2, 360/4000) #established encoder 1
print('Encoder 1 Initialized')

# pyb.delay(25)
# myEncoder2 = FSM_encode.EncoderDriver(pyb.Pin.cpu.C6, pyb.Pin.cpu.C7, 8, 1, 2) #established encoder 1
# print('Encoder 2 Initialized')

pyb.delay(25)
myMotor1 = moset.MotorDriver(pyb.Pin.cpu.A15, pyb.Pin.cpu.B4, pyb.Pin.cpu.B5, 3, 1, 2)
print('Motor 1 Initialized')

# pyb.delay(25)
# myMotor2 = MotorDriver(None, pyb.Pin.cpu.B0, pyb.Pin.cpu.B1, 3, 3, 4)
# print('Motor 1 Initialized')

pyb.delay(25)
myController1 = SC_closedloop.ClosedLoop(myMotor1, myEncoder1, myuart)
print('Controller 1 Activated')


##----------------------Run Finite State Machine Tasks----------------------##

while True:
    #check for any requests and decode if true
    if myuart.any() != 0:
        pyb.delay(3) #let the full string input in!!
        
        #read goodies from Spyder/UART
        rawinput = myuart.readline()
        newInput = str(rawinput.decode('ascii'))
        
        if newInput != lastInput:
            try:
                inputPieces = newInput.strip().split(','); #unpack 
                cmd = inputPieces[0]
                target = inputPieces[1]
                data1 = float(inputPieces[2])
                data2 = float(inputPieces[3])

            except IndexError:
                cmd = '-'
            
            except ValueError:
                cmd = '-'
                
            if target == '2':
                print('Pair 2 coming soon :)')
                cmd = -'-'

            if cmd == 'z': #if z is pressed
                print(myEncoder1.setPos())
                
            elif cmd == 'p': #if p is pressed
                print(myEncoder1.getPos())
                
            elif cmd == 'd': #if d is pressed
                print(myEncoder1.showDelta())
                
            elif cmd == 'g': #if g is pressed
                myEncoder1.recordData()
                
            elif cmd == 's':
                myEncoder1.recording = 0
                print('Recording Stopped')
                
            elif cmd == 'm':
                myMotor1.Forward(int(data1))
            
            elif cmd == 'k':
                myController1.Kp = data1
                
            elif cmd == 'i':
                myController1.Ki = data1
            
            elif cmd == 'x':
                myController1.P_running = 1
                DesiredSpeed = data1
                myController1.Ki = 0.0
                myEncoder1.recordData()
            
            elif cmd == 'f':
                if Syncing == False:
                    myController1.P_running = 1
                    myMotor1.Coast()
                    pyb.delay(10)
                    myEncoder1.curr_pos = 0
                    Syncing = True
                    print('Tracking Algorithm Ready')
                    myEncoder1.recordData()
                elif Syncing == True:
                    print('Tracking Algorithm Finished')
                    print('J value: ', myController1.Jval)
                    Syncing = False
            
            elif cmd == '~':
                DesiredSpeed = data1
                DesiredPosition = data2
                myController1.Jcalcs(DesiredSpeed, DesiredPosition, myEncoder1.curr_speed, myEncoder1.curr_pos*myEncoder1.gear_ratio)
                
            elif cmd == '-':
                pass

        lastInput = newInput
    

    #update tasks as usual
    myEncoder1.run()
    myController1.PIcontrol(DesiredSpeed, myEncoder1.curr_speed, DesiredPosition, myEncoder1.curr_pos*myEncoder1.gear_ratio)

    # myEncoder2.run()
