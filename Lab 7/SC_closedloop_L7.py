# -*- coding: utf-8 -*-
"""
@file SC_closedloop.py

@package Lab7

@brief Closed Loop Proportional Controller

@details This source code is meant to be robust and work for multiple uses. The desired variable can
be set, and the proportional gain can also be set. This is very useful. The code can also set limits,
we do not want to oversaturate any components! Just make sure to keep
track of units throughout! This code was apart of \ref Lab6 and \ref Lab7
\n Source Code: https://bitbucket.org/BENNYB0P/mechatronics/src/master/Lab%207/SC_closedloop.py
\n\n
The code was tested in the previous labratory, \ref Lab6, where it was tuned to a step function. In this lab, we are
dealing with a more complicated ramp input. \n \n We also wanted to measure the performance of our control
system, that is why the Jcalc function has been included. The controller was very useful in the test results shown below
when Spyder communicated syncronously with the Nuclueo.

\n \n
The spyderspace communicated with the Nucleo and produced the following syncronized
movements. The orange line is the Spyderspace's request, and the blue line is the actual.

\image html lab7_test_J484478.png

\n The J value was 484,000 for this test. With Kp = 0.12 and Ki = 0.012

@author: Ben Presley

@date December 4, 2020

@copyright Consent of Ben Presley before use.
"""

class ClosedLoop:
    ''' 
    @brief basic proportional controller which functions can be called
    @details This class was developed for lab 6, where the encoder will provide sensory
    information on teh state of the motor. This system is set up to work for any
    circumstance of control -- whether it be control of position or velocity.
    
    '''
    
    def __init__(self, myMotor, myEncoder, myuart):
        '''
        @brief Initializes the ClosedLoop Controller object=
        @details We want to bring in the other objects that is apart of the coontrol
        system so they can be referenced at any time. This will save us a lot of time and
        headache trying to pass their parameters in with other, more difficult methods.
        '''
        self.myEncoder = myEncoder
        self.myMotor = myMotor
        self.myuart = myuart
        self.PWM_max = 100 #magnitude of 100, can be negative or positive
        self.PWM_min = -100
        
        self.P_running = 0
        
        self.Jval = 0.0
        self.Jvali = 0.0
        self.count = 0
        
        #self.setKp() #call the setKp function
        
        #self.setDesired() #call setDesired function
        
        #Set Kp to expirementally determined Kp
        self.Kp = 0.12
        self.Ki = 0.012

    
    def PIcontrol(self, DesiredP, ActualP, DesiredI, ActualI):
        '''
        @brief Simple proportional gain controller
        @details The simple controller works by calculating the error, the difference 
        between the desired and actual speed of the output shaft, and then multiplies the
        calculated error by the constant gain, Kp. Kp has units of %PWM/RPM. The value is tuned
        to perfection where we do not have to much overshoot, or too much steady state error.
        The controller is kept simple to keep the code running fast, and was converted into
        a PI controller fairly easily by adding a constant gain value for the difference in desired position
        versus actual position. I did find it necessary for this lab to add in the positional/integral
        feedback gain.
        '''
        if self.P_running == 1:
            self.ErrorP = DesiredP - ActualP
            self.ErrorI = DesiredI - ActualI
            self.proposedDuty = self.ErrorP*self.Kp + self.ErrorI*self.Ki
            self.duty = self.clamp(self.proposedDuty)
            self.myMotor.AutoDuty(self.duty,ActualP)
        else:
            pass
            
    def Jcalcs(self, DesiredVel, DesiredPos, ActualVel, ActualPos):
        '''
        @brief This function measures the performance of my control system
        @details The function is tasked with quantifying the success of my
        control system. The control system is following a transient desired position and velocity
        profile assigned in \ref Lab7 -- this profile has a mix of step and ramp inputs in which
        the control system much adapt.\n \n The performance of the system is measured with the
        equation below: \n
        \image html "Jperformance_eq.png"
        \n This function adds up the error for each iteration, k, of time. The value of J
        should remain close to 0 and not become too large. Essentially, the value of J is the
        average error of the system across the time it was run. We do not want this value to become too large.
        This method of measuring error also serves the purpose of ensuring our output shaft position and velocity
        calculations are linked, linear, and fast.
        
        '''
        self.Jvali = self.Jvali + (DesiredVel - ActualVel)**2 + (float(DesiredPos - ActualPos))**2
        self.count = self.count + 1
        self.Jval = self.Jvali/self.count

        
    def clamp(self, n):
        '''
        @brief Limits values based on min and max values
        @details This clamp is very important for applications like PWM because
        we cannot oversaturate the duty. This function has been designed to run as fast
        as possible to make sure it does not slow down the code.
        '''
        
        if n < self.PWM_min:
            return self.PWM_min
        elif n > self.PWM_max:
            return self.PWM_max
        else:
            return n
        
        