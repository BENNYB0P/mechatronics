'''
@file Elevator.py

This file serves as an example implementation of a finite-state-machine using
Python. The example will implement some code to control an imaginary elevator
system for a 5 story building.

The user has a panel of buttons for each floor.

@package Elevator



'''

import time

class TaskElevator:
    '''
    @brief      A finite state machine to control 5 story elevator.
    @details    This class implements a finite state machine to control the
                operation of an elevator with user input.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
    ## Constant defining State 1
    S1_LVL_1            = 1    
    
    ## Constant defining State 2
    S2_MOVE_1_2         = 2    
    
    ## Constant defining State 3
    S3_LVL_2            = 3
    
    ## Constant defining State 4
    S4_MOVE_2_3         = 4
    
    ## Constant defining State 5
    S5_LVL_3            = 5
    
    ## Constant defining State 6
    S6_MOVE_3_4         = 6
    
    ## Constant defining State 7
    S7_LVL_4            = 7
    
    ## Constant defining State 8
    S8_MOVE_4_5         = 8
    
    ## Constant defining State 9
    S9_LVL_5            = 9
    
    ## Constant defining State 10
    S10_MOVE_5_4        = 10
    
    ## Constant defining State 11
    S11_MOVE_4_3        = 11
    
    ## Constant defining State 12
    S12_MOVE_3_2        = 12
    
    ## Constant defining State 13
    S13_MOVE_2_1        = 13

    
    
    def __init__(self, interval, Button1, Button2, Button3, Button4, Button5, Panel, Motor):
        '''
        @brief            Creates an elevator object.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The button object used for LVL 1
        self.Button1 = Button1
        
        ## The button object used for LVL 2
        self.Button2 = Button2

        ## The button object used for LVL 3
        self.Button3 = Button3
        
        ## The button object used for LVL 4
        self.Button4 = Button4

        ## The button object used for LVL 5
        self.Button5 = Button5
        
        ## The motor object "wiping" the wipers
        self.Motor = Motor
        
        ## The panel object acts as teh UI
        self.Panel = Panel
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        # State time conditions
        self.curr_time = time.time()
        if self.curr_time > self.next_time:
            
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_LVL_1)
                print(str(self.runs) + ': S0 - POWER ON')
                
            
            
            elif(self.state == self.S1_LVL_1):
                # Run State 1 Code
                self.Motor.Stop()
                print(str(self.runs) + ': S1 - LVL 1')
                
                self.Panel.GetLVLwant()
                
                if(self.Panel.LVLwant> 1):
                    self.transitionTo(self.S2_MOVE_1_2)
                else:
                    self.transitionTo(self.S1_LVL_1)
            
            
            
            elif(self.state == self.S2_MOVE_1_2):
                # Run State 2 Code
                self.Motor.Lift()
                print(str(self.runs) + ': S2 - LIFTING 1 -> 2')
                
                if(self.Panel.LVLwant > 2):
                    self.transitionTo(self.S4_MOVE_2_3)
                elif(self.Panel.LVLwant == 2):
                    self.transitionTo(self.S3_LVL_2)
            
            

            elif(self.state == self.S3_LVL_2):
                # Run State 3 Code
                self.Motor.Stop()
                print(str(self.runs) + ': S3 - LVL 2')    
                
                self.Panel.GetLVLwant()
                
                if(self.Panel.LVLwant > 2):
                    self.transitionTo(self.S4_MOVE_2_3)
                elif(self.Panel.LVLwant < 2):
                    self.transitionTo(self.S13_MOVE_2_1)
                else:
                    self.transitionTo(self.S3_LVL_2)

            
            
            elif(self.state == self.S4_MOVE_2_3):
                # Run State 4 Code
                self.Motor.Lift()
                print(str(self.runs) + ': S4 - LIFTING 2 -> 3')
                
                if(self.Panel.LVLwant > 3):
                    self.transitionTo(self.S6_MOVE_3_4)
                elif(self.Panel.LVLwant == 3):
                    self.transitionTo(self.S5_LVL_3)

                

            elif(self.state == self.S5_LVL_3):
                #Run State 5 Code
                self.Motor.Stop()
                print(str(self.runs) + ': S5 - LVL 3')    
                
                self.Panel.GetLVLwant()
                
                if(self.Panel.LVLwant > 3):
                    self.transitionTo(self.S6_MOVE_3_4)
                elif(self.Panel.LVLwant < 3):
                    self.transitionTo(self.S12_MOVE_3_2)
                else:
                    self.transitionTo(self.S5_LVL_3)
                
                
            
            elif(self.state == self.S6_MOVE_3_4):
                # Run State 6 Code
                self.Motor.Lift()
                print(str(self.runs) + ': S6 - LIFTING 3 -> 4')
                
                
                if(self.Panel.LVLwant > 4):
                    self.transitionTo(self.S8_MOVE_4_5)
                elif(self.Panel.LVLwant == 4):
                    self.transitionTo(self.S7_LVL_4)
                
                
            
            elif(self.state == self.S7_LVL_4):
                #Run State 7 Code
                self.Motor.Stop()
                print(str(self.runs) + ': S7 - LVL 4')    
                
                self.Panel.GetLVLwant()
                
                if(self.Panel.LVLwant > 4):
                    self.transitionTo(self.S8_MOVE_4_5)
                elif(self.Panel.LVLwant< 4):
                    self.transitionTo(self.S11_MOVE_4_3)
                else:
                    self.transitionTo(self.S7_LVL_4)
                
                
            
            elif(self.state == self.S8_MOVE_4_5):
                # Run State 8 Code
                self.Motor.Lift()
                print(str(self.runs) + ': S8 - LIFTING 4 -> 5')
                
                self.transitionTo(self.S9_LVL_5)
                
                
            
            elif(self.state == self.S9_LVL_5):
                #Run State 9 Code
                self.Motor.Stop()
                print(str(self.runs) + ': S9 - LVL 5')    
                
                self.Panel.GetLVLwant()
                
                if(self.Panel.LVLwant > 5):
                    self.transitionTo(self.S9_LVL_5)
                elif(self.Panel.LVLwant < 5):
                    self.transitionTo(self.S10_MOVE_5_4)
                else:
                    self.transitionTo(self.S9_LVL_5)
            
            
            
            elif(self.state == self.S10_MOVE_5_4):
                #Run State 10 Code
                self.Motor.Lower()
                print(str(self.runs) + ': S10 - LOWERING 5 -> 4')
                
                if(self.Panel.LVLwant == 4):
                    self.transitionTo(self.S7_LVL_4)
                elif(self.Panel.LVLwant < 4):
                    self.transitionTo(self.S11_MOVE_4_3)
            
            
            
            elif(self.state == self.S11_MOVE_4_3):
                #Run State 11 Code
                self.Motor.Lower()
                print(str(self.runs) + ': S11 - LOWERING 4 -> 3')
                
                if(self.Panel.LVLwant == 3):
                    self.transitionTo(self.S5_LVL_3)
                elif(self.Panel.LVLwant < 3):
                    self.transitionTo(self.S12_MOVE_3_2)
                    
                    
            
            elif(self.state == self.S12_MOVE_3_2):
                #Run State 12 Code
                self.Motor.Lower()
                print(str(self.runs) + ': S12 - LOWERING 3 -> 2')
                
                if(self.Panel.LVLwant == 2):
                    self.transitionTo(self.S3_LVL_2)
                elif(self.Panel.LVLwant < 2):
                    self.transitionTo(self.S13_MOVE_2_1)
            
            
            
            elif(self.state == self.S13_MOVE_2_1):
                #Run State 13 Code
                self.Motor.Lower()
                print(str(self.runs) + ': S13 - LOWERING 2 -> 1')
                
                self.transitionTo(self.S1_LVL_1)
            
            
            else:
                # Invalid state code (error handling)
                pass
            
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        

class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                imaginary driver to turn on or off the wipers. As of right now
                this class is implemented using "pseudo-hardware". That is, we
                are not working with real hardware IO yet, this is all pretend.
    '''
    
    def __init__(self, pin, floor_designation):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
               floor_designation which button the floor represents
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        self.myLVL = floor_designation
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def LightOn(self):
        '''
        @brief      Lights up designated button.
        @details    Since there is no hardware attached this method, the button
                    will not be able to send any info to the panel because
                    it can not be physcially pressed.
        @return     A boolean representing the state of the button.
        '''
        print('Button {} lights up',self.myLVL)

    def LightOff(self):
        '''
        @brief      turns off designated button
        @details    Opposite of the LightOn function
        '''
        print('Button {} turns off',self.myLVL)


class PanelBox:
    '''
    @brief     A panel class
    @details   This class represents a panel that the users would interact with
               in the cab of the elevator. Since we are not working with
               hardware, it will just be a console input.
    @return    An integer indicating the desired destination level.
    '''
    def __init__(self):
        '''@brief  Creates a Panel object.'''
        pass
    
    def GetLVLwant(self):
        '''
        @brief      Ask user what floor they want to go to
        @details    The input must be a integer for one of the floor levels
        '''
        while True:
            try:
                self.LVLwant = int(input('What floor would you like to travel to?: '))
                print('')
                if(self.LVLwant < 1):
                    print('Access denied! Basement levels for Ben Presley only!')
                    ValueError
                elif(self.LVLwant > 5):
                    print('Easy there... this aint no rocket ship!')
                    ValueError
                else:
                    return self.LVLwant
                    break
            except ValueError:
                print('Click on the buttons, silly goose! They are labeled 1, 2, 3, 4, and 5!')

class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to make the wipers
                wipe back and forth.
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def Lift(self):
        '''
        @brief Motor rotates to raise the elevator
        '''
        print('Motor Control System: Lift 1 Story')
    
    def Lower(self):
        '''
        @brief Motor rotates to lower the elevator
        '''
        print('Motor Control System: Lower 1 Story')
    
    def Stop(self):
        '''
        @brief Stops the motor
        '''
        print('Motor Control System: Reduce Speed to stop')


# This code would be in your main project most likely (main.py)
        
# Creating objects to pass into task constructor
Panel = PanelBox()         #Panel with a bunch of buttons
Button1 = Button('PB1',1)  #Button to go to Level 1
Button2 = Button('PB2',2)  #Button to go to Level 2
Button3 = Button('PB3',3)  #Button to go to Level 3
Button4 = Button('PB4',4)  #Button to go to Level 4
Button5 = Button('PB5',5)  #Button to go to Level 5
Motor = MotorDriver()

# Creating a task object using the button and motor objects above
task1 = TaskElevator(0.1, Button1, Button2, Button3, Button4, Button5, Panel, Motor)

# Run the tasks in sequence over and over again
for N in range(10000000): # effectively while(True):
    task1.run()
#    task2.run()
#    task3.run()