# -*- coding: utf-8 -*-
"""

@file blu.py

@package Lab5

@brief This is a full script that demonstrates \ref Lab5

@details Lab 5 consisted of developing a simple FSM that connected the Bluetooth module to
the Nucleo in a robust fashion. The only thing we are controlling tioday is the LED on the Nucleo. 
Although simple, establishing confidence with the Bluetooth module will be crtical for further 
mechatronics learning. I was also tasked with developing a simple app with Thunkable that could be implemented
onto my personal iPhone. The iPhone could then act as the interface solemente.

\n \n Script Code Link: https://bitbucket.org/BENNYB0P/mechatronics/src/master/Lab%205/blu.py

\n \n SC_blink Link: https://bitbucket.org/BENNYB0P/mechatronics/src/master/Lab%204/SC_blink.py

\n \n Thunkable App Code: https://x.thunkable.com/copy/13e15ceac999be0c26d0964da21f4edc

\n \n The finite state machine diagram:  \image html "lab5_FSM.png"

\n \n The task diagram:  \image html "lab5_task.png"

@date Nov 11 2020

@author: Ben Presley
"""

#------------------------------General Setup----------------------------------#
#import system files
import pyb
from pyb import UART
import SC_blink


## Define Serial Port for Bluetooth Connection
bluart = UART(3, 9600) #buadrate is default to 9600
bluart.init(9600)
print('UART 3 Initialized at 9600 buadrate')


#---------------------------Finite State Machine------------------------------#
class Bluetool:
    '''
    @brief Class that reads and writes with a bluetooth module
    
    @details This is set up as a finite state machine to allow for easy communication
    with a bluetooth module while not prohibitting the tasks running on the Nucleo. Please
    see the image below for the task diagram.
    
    \image html "lab4_demo.png"
    '''
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
    ## Constant defining State 1
    S1_READ             = 1    
    
    ## Constant defining State 2
    S2_WRITE            = 2    
    
    ## Constant defining State 3
    S3_COMMAND          = 3
    

    def __init__(self):
        '''
        @brief            Creates an bluetooth communication object.
        '''
        
        #define initial state
        self.state = self.S0_INIT
        
        #Define receieved variable
        self.Recieved = 0
        
        #Define run speed
        self.interval = 5 #[ms]
        
        #Define current time
        self.curr_time = pyb.millis()
        
        #Define next time
        self.next_time = self.curr_time + self.interval
        
        
    def run(self):
        ''' 
        @brief run the FSM
        '''
        if(self.curr_time >= self.next_time):
            
            #Run Initialization sate
            if(self.state == self.S0_INIT):
                self.transitionTo(self.S1_READ)
        
        
            #Run Read Sequence
            elif(self.state == self.S1_READ):
                if(bluart.any()!=0): #if there is something on the bus
                    self.Recieved = bluart.readline()
                    print('Bluetooth sent: ',self.Recieved)
                    self.transitionTo(self.S3_COMMAND)

            
            
            #Run Write Sequence -- this is still in development for next time.
            #Not required for lab 5.
            elif(self.state == self.S2_WRITE): 
                self.sendit = input('String to send: ')
                bluart.write(str(self.sendit))
                self.transitionTo(self.S1_READ)
                
            
            #Run Command Sequence
            elif(self.state == self.S3_COMMAND):
                #Determine type of input
                if(self.Recieved == 'ON'):
                    myLED.TaskBlink.blinkduty = 100
                elif(self.Recieved == 'OFF'):
                    myLED.TaskBlink.blinkduty = 0
                else: #the user must have inputted something into the text box
                    try: #make sure it is an integer
                        self.desiredFreq = int(self.Recieved)
                        myLED.TaskBlink.changePeriod(self.desiredFreq)
                    except ValueError: #error handle
                        print('Bluetooth command not an integer') 
                #Always go back to state 1
                self.transitionTo(self.S1_READ)
        
            else: #error handle
                pass
            
        self.next_time = self.curr_time + self.interval
        
        
    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        '''
        self.state = newState
        

#-----------------------------Establish Objects-------------------------------#
myLED = SC_blink.TaskBlink(1,'Blink','Physical') #1 second period iluminating in a sinusoidal pattern on the physical LED
myBlu = Bluetool()

#---------------------------------Run Tasks-----------------------------------#
while True:
    myLED.run()
    myBlu.run()
    

