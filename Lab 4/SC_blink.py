"""
@file SC_blink.py

@package BlinkingLEDs

@details This source code provides the tools and code necessary to blink
an LED at various patterns and periods. The code utilizes pyb, math, and PWM
functions that are built into Python. The code is referenced by several code packages
in my mechatronics course.


@date Nov 04 2020

@author: Ben Presley
"""

import pyb
import math

class TaskBlink:
    '''
    @brief finite-state machine for blinking an LED
    @details This finite-state machine can handle patterns of LED blinking. It
    utilizes PWM, pyb, and math functions built into Python.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
    ## Constant defining State 1 - Blinking on and off
    S1_BLINK            = 1    
    
    ## Constant defining State 2 - Sinusoidal brightness
    S2_SINE             = 2    
    
    ## Constant defining State 3 - Sawtooth brightness
    S3_SAWTOOTH         = 3
    
    ## Constant defining State 3 - Turn LED off
    S4_OFF              = 4
    
    
    def __init__(self,Period,BlinkType,LEDType):
        '''
        @brief Established the parameters of the system
        '''
        ## Set state to zero
        self.state = self.S0_INIT
        
        ## Save the period input to self
        self.Period = Period
        
        ## Save the LEDType input to self
        self.LEDType = LEDType
        
        ## Dedicate/Create LED for this Task (error handling included)
        if(self.LEDType == 'Physical'):
            self.myLED = LED_Driver(self.LEDType)
            pyb.delay(50)
            print('LED hardware driver prompted')
            pyb.delay(50)
        elif(self.LEDType == 'Virtual'):
            self.myLED = LED_Driver(self.LEDType)
            print('Virtual LED initiated')
        else:
            print('Error - that type of LED does not exist - AI has overthrown you and automatically set it to virtual... just like the future of humankind')
            self.LEDType = 'Virtual'
            self.myLED = LED_Driver(self.LEDType) #error handling
            
        ## Save the BlinkType to self
        self.BlinkType = BlinkType
        
        ## The timestamp for the first iteration
        self.start_time = pyb.millis()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + 2000
        
    def run(self):
        '''
        @brief Tasks run within state-machine and allow for multitasking
        @details Each pattern is set up to run at a specific frequency and
        pattern as outlined by the user input.
        '''
        # State time conditions
        self.curr_time = pyb.millis()
        
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                if(self.BlinkType == 'Blink'):
                    self.transitionTo(self.S1_BLINK)
                    self.status = 0
                    self.blinkduty = 100
                elif(self.BlinkType == 'Sine'):
                    self.transitionTo(self.S2_SINE)
                elif(self.BlinkType == 'Saw'):
                    self.transitionTo(self.S3_SAWTOOTH)
                else:
                    self.transitionTo(self.S4_OFF)
                    

                
            elif(self.state == self.S1_BLINK):
                if(self.status == 0): #if its off turn it on.
                    self.myLED.SetBrightness(100)
                    self.status = 1
                    self.next_time = self.curr_time + (self.Period*1000)
                else: #if its on (or other) turn it off.
                    self.myLED.SetBrightness(0)
                    self.status = 0
                    self.next_time = self.curr_time + self.Period*1000
                        
                        
                        
            elif(self.state == self.S2_SINE):
                self.Duty_Calc = 50+50*math.sin((self.curr_time-self.start_time)*2*3.14159/(self.Period*1000))
                self.myLED.SetBrightness(self.Duty_Calc)
                self.next_time = self.curr_time + 5 #refresh value every 5 ms.
                
            
            else:
                pass
            
                
                
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
        
    def changePeriod(self, FrequencyDesired):
        '''
        @brief Changes the frequency of the 
        '''
        self.Period = 1/FrequencyDesired
        
        
        
        
class LED_Driver:
    '''
    @brief      An LED light driver for virtual and physical LEDs
    @details    This class controls an LED object. The LED can be represented
                physcically or virtually within the console. This is only necessary
                because their is a mixture of real and virtual LEDs in this lab
    '''
    
    def __init__(self,LED_Type):
        '''
        @brief Creates a LED Object
        '''
        self.LED_Type = LED_Type
        
    
    def SetBrightness(self, given_Duty):
        '''
        @brief Sets power to bulb
        '''
        if(self.LED_Type == 'Virtual'):
            print('Virtual LED set to ',given_Duty,'%')
        elif(self.LED_Type == 'Physical'):
            LED_Assignment(given_Duty)
        else:
            print('LED_Driver encountered a black hole')



def LED_Assignment(given_Duty):
    pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
    tim2 = pyb.Timer(2, freq = 20000)
    t2ch1 = tim2.channel(1,pyb.Timer.PWM, pin=pinA5)
    t2ch1.pulse_width_percent(given_Duty)
