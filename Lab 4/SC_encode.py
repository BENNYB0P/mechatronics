"""
@file SC_encode.py

@package Lab4

@breif Source Code for encoder objects

@details This source code provides the tools necessary to read
data from a motor's encoder. The code utilizes pyb and PWM
functions that are built into Python. There is also a function to relay
time responses into the code to allow for easy recording and graphing as
we have seen in lab 4. The recording resolution is set to 0.5 seconds
but can be easily adjusted as desired. \n
Source Code: https://bitbucket.org/BENNYB0P/mechatronics/src/master/Lab%203/SC_encode.py

@date Oct 21 2020

@author: Ben Presley
"""

import pyb
from pyb import UART
myuart = UART(2)

class EncoderDriver:
    '''
    @brief Communicates with hardware to get encoder data 
    @details This driver is key for accessing data from the motor's encoder
    which will be key for many projects in this course/profession.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
    ## Constant defining State 1 - Read Timer
    S1_UPDATE           = 1    
    
    ## Constant defining State 2 - Interpret UPDATE
    S2_getDelta         = 2    
    
    ## Constant defining State 3 - Return Position
    S3_getPos           = 3
    
    ## Constant defining State 4 - Turn LED off
    S4_setPos           = 4
    
    
    def __init__(self):
        '''
        @brief Established the parameters of the system
        '''
        ## Set state to zero
        self.state = self.S0_INIT
        
        ## The timestamp for the first iteration
        self.start_time = pyb.millis()
        
        self.curr_pos = 0 #original location
        self.Delta = 0 #orininal delta
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + 200
        
        ## Set recording data state to False
        self.recording = 0
        
        ## Recording Resolution
        self.Resolution = 500 #[ms], please do not go below 50 ms.
        
    def run(self):
        '''
        @brief Tasks run within state-machine and allow for multitasking
        @details The encoder must account for overflow and underflow. The
        program takes advantage of the timer function for pyb. The mode was 
        set to ENC_AB for easiest implementation of clockwise and
        counterclockwise movements. Note that the position is still just the
        ticks of the timer. The ticks have not been calibrated to degrees yet.
        The code also has a recording function built in!
        '''
        # State time conditions
        self.curr_time = pyb.millis()
        
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code - set Up Timer!
                self.tim = pyb.Timer(3)
                self.tim.init(prescaler=1,period=0xFFFF)
                self.tim.channel(1,pin=pyb.Pin.cpu.A6,mode=pyb.Timer.ENC_AB)
                self.tim.channel(2,pin=pyb.Pin.cpu.A7,mode=pyb.Timer.ENC_AB)
                self.oldTick = self.tim.counter() #original location
                self.transitionTo(self.S1_UPDATE)
                
            elif(self.state == self.S1_UPDATE):
                # Run State 1 Code - Get info from Timer
                self.newTick = self.tim.counter() #simply snag data
                self.transitionTo(self.S2_getDelta)
                
            
            elif(self.state == self.S2_getDelta): #interpret data
                # Run State 2 Code - Interpert and add to Position
                self.Delta = self.newTick - self.oldTick
                
                if(self.Delta == 0):
                    self.transitionTo(self.S1_UPDATE)
                elif(self.Delta < (-0xFFFF/2)): #overflow case
                    self.curr_pos = self.curr_pos + (0xFFFF - self.oldTick) + self.newTick
                    self.transitionTo(self.S1_UPDATE)
                elif(self.Delta > (0xFFFF/2)): # underflow case
                    self.curr_pos = self.curr_pos - ((0xFFFF - self.newTick) + self.oldTick)
                    self.transitionTo(self.S1_UPDATE)
                else: #normal case
                    self.curr_pos = self.curr_pos + self.Delta
                ## RECORDING DATA ##
                if(self.recording == 1):
                    if(self.curr_time - self.Resolution >= self.record_tlast): #record at resolution
                        print(str((self.curr_time - self.record_tinit)/1000) + ', '+ str(self.curr_pos - self.record_datainit))
                        self.record_tlast = self.curr_time
                        
                    if((self.curr_time - self.record_tinit) > 10100): #after ten seconds of recording
                        self.recording = 0 #set recording to false
                        print('Recording Finished') #state the recording has stopped.
                        
                self.oldTick = self.newTick #redefine what the old tick is now
            
    def getPos(self):
        '''
        @brief return current position
        '''
        return str('Current Postion: ' +str(self.curr_pos))
    
    def setPos(self):
        '''@brief sets position to zero'''
        self.curr_pos = 0
        return str('Position reset to zero')
        
    def showDelta(self):
        '''
        @brief return current delta
        '''
        return str('Current Delta: '+str(self.Delta))
        
        
    def recordData(self):
        ''' @brief returns a data list of time vs position for 10 seconds'''
        self.recording = 1
        self.record_tinit = self.curr_time
        self.record_tlast = self.curr_time
        self.record_datainit = self.curr_pos
        print('Recording Started')


    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState