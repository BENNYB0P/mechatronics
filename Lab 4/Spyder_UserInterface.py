"""
@file Spyder_UserInterface.py

@package Lab4

@breif Spyder script to interact real time with Nucleo

@details This scripts incoroporates the serial functionality to communicate
with the Nucleo. The nucleo is tasked to run the \ref main_L4.py script -- which
manages the Sinusoidal LED pattern and keeps track of the encoder. The encoder
has a new recording function which allows the data to be plotted. The plotting
functionality is very nice and robust for this application.  \n
Source Code: 

@date Oct 21 2020

@author: Ben Presley
"""

import serial #for communication with Nucleo
import keyboard #for Spyder detecting keyboard presses.
import time #timekeeping
import matplotlib.pyplot as plt #for plotting
import sys #for exit function only

ser = serial.Serial(port='COM3',baudrate=115273,timeout=.1) #comms with Nucleo

class UIrequest:
    '''
    @brief User Interface for robust transfer of Inputs and Output 
    @details This code allows the user to input keystrokes to Spyder which sends commands
    to the Nucleo. The code will also respond to outputs from the Nucleo and 
    harvest/store data when necessary. For lab 0x04 there is also a plotting
    functionality incorporated into the code.
    
    '''
    
    ## Initialization state
    S0_INIT           = 0
    
    ## Communication state - runs forever
    S1_COMMUNICATE    = 1
    
    def __init__(self):
        '''
        @brief Established the parameters of the input system
        '''
        ## Set state to zero
        self.state = self.S0_INIT
        
        ## Run communication sequence every 10 ms
        self.Rate = 10
        
        ## The timestamp for the first iteration
        self.start_time = time.time()*1000
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.Rate
        
        
        #Make sure recording value is False
        self.Recording = False
        self.time_plot = [0, 0]
        self.degs_plot = [0, 0]
        self.GearRatio = 360/13/50 # [deg/tick]
        self.AlreadyRecorded = False
        
        #Define old out and blanks
        self.oldout = 'GoPC\r\n'
        self.blank = ''
        
        ## Confirm code is running
        print('User Interface Engaged. Waiting for connection to Nucleo\n\nPress reset button on Nucleo to start...')
        
    
    
    
    def run(self):
        # State time conditions
        self.curr_time = time.time()*1000
        
        
        if self.curr_time > self.next_time:
            
            #State 0 Code - runs initially
            if(self.state == self.S0_INIT):
                self.checkgo = self.readLine()
                if(self.checkgo=='GoPC\r\n'):
                    print('Successful connection to Nucleo!')
                    self.transitionTo(self.S1_COMMUNICATE)

                
            #State 1 Code - runs forever repeating
            elif(self.state == self.S1_COMMUNICATE):
                #Send any inputs to Nucleo
                if(self.Recording == False):
                    if(keyboard.is_pressed('g')):
                       if(self.AlreadyRecorded == False):
                           self.sendLine('g')
                       else:
                           print('You have already recorded data')
                    
                    elif(keyboard.is_pressed('p')):
                        self.sendLine('p')
                    elif(keyboard.is_pressed('z')):
                        self.sendLine('z')
                    elif(keyboard.is_pressed('d')):
                        self.sendLine('d')
                    elif(keyboard.is_pressed('r')):
                        ser.close()
                        sys.exit()
                if(keyboard.is_pressed('s')):
                    if(self.AlreadyRecorded == False):
                        self.sendLine('s')

                
                #Recieve any outputs from Nucleo
                self.out = self.readLine()
                if(self.out != self.oldout and self.out != self.blank):
                    print(self.out)
                    
                    if(self.out == 'Recording Finished\r\n' or self.out == 'Recording Stopped\r\n'):
                        self.Recording = False
                        self.AlreadyRecorded = True
                        self.PlotResults(self.time_plot, self.degs_plot)
                        print('To exit and render results, press "r"')
                        
                    if(self.Recording == True):
                        self.linestring = self.out
                        self.cleanstring = self.linestring.strip('\r\n')
                        self.line_list = self.cleanstring.strip().split(', ')
                        self.time_plot.append(float(self.line_list[0]))
                        self.degs_plot.append(float(self.line_list[1])*self.GearRatio)
                    
                    if(self.out == 'Recording Started\r\n'):
                        self.Recording = True
                        
                        
                self.oldout = self.out #define olgd data so we dont overprint it
            
            self.next_time = self.curr_time+self.Rate #run again at rate
            
        else:
            pass
                
    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        '''
        self.state = newState
        
    def sendLine(self, string):
        ''' @brief Sends a line of code through the serial bus
        @details I used ascii encoding because it seems to behave nicely with
        the back ends use of UART -- and we were only dealing with sending one
        character'''
        self.sendit = str(string).encode('ascii')
        ser.write(self.sendit)
        
    def readLine(self):
        ''' @brief Recieves the most recent console line in the Nucleo
        @details I used -utf-8 decoding because it seemed to behave nicely with
        the front end (Spyder) and transfer large strings'''
        self.readit = str(ser.readline().decode('-utf-8'))
        return self.readit

    def PlotResults(self, xlist, ylist):
        ''' @brief plots given data series.'''
        x_vals = xlist
        y_vals = ylist
    
        plt.plot(x_vals, y_vals)
        plt.xlabel('Time, [s]')
        plt.ylabel('Position of Output Shaft, [deg]')
        plt.grid()
        
        
    

##--------------------------------------------------------------------------##
# Run the script from Spyder
myInterface = UIrequest()

for N in range(100000000): # effectively while(True):
    myInterface.run()
    
ser.close() #close serial connection properly
