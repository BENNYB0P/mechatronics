"""
@file main_L4.py

@package Lab4

@breif Source Code for encoder objects

@details This main code manages the function and scripts for package \ref Lab4. 
The scripts are set up as finite state machines to allow for multitasking. The LED
will begin to shine in a sinusoidal pattern to indicate that the back end (nucleo) is
running smoothly. The Nucleos reset button will be useful when you start up the
user interface code: \ref Spyder_UserInterface.py \n
Source Code: 

@date Oct 21 2020

@author: Ben Presley
"""

## Import necessary published
import pyb #useful library
from pyb import UART #for communication with Spyder
import os #useful library


## Import my source code files (that must already be put on Nucleo)
import SC_encode
import SC_blink

## Define Serial Port for Handshake
myuart = UART(2)

## Send go ahead to PC
print('GoPC') #This string will prompt Spyder to move forward
pyb.delay(1000) #give Spyder a second to transfer into operation state
print('Nucleo reporting for duty.')

#----------------------------------------------------------------------------#
  #This code runs when LED begins to shine - make sure Spyder is connected#

## Define Tasks - these run on the Nucleo!
pyb.delay(25)
myLED = SC_blink.TaskBlink(5,'Sine','Physical') #5 second period iluminating in a sinusoidal pattern on the physical LED
print('LED initialized')
pyb.delay(25)
myEncoder = SC_encode.EncoderDriver() #established encoder
print('Encoder Initialized')

## Run Finite State Machine Tasks
while True:
    #check for any special requests
    if myuart.any() !=0:
        cmd = myuart.readchar()
        if cmd == 'z' or cmd == 122: #if z is pressed
            print(myEncoder.setPos())
        elif cmd == 'p' or cmd == 112: #if p is pressed
            print(myEncoder.getPos())
        elif cmd == 'd' or cmd == 100: #if d is pressed
            print(myEncoder.showDelta())
        elif cmd == 'g' or cmd == 103: #if g is pressed
            myEncoder.recordData()
        elif cmd == 's' or cmd == 115:
            myEncoder.recording = 0
            print('Recording Stopped')
        else:
            print('Nucleo has no function for entry: ', cmd)


    #update tasks as usual
    myLED.run()
    myEncoder.run()

