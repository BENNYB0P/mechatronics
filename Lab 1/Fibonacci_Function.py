'''
@file Fibonacci_Function.py

@package Fibonacci

@brief Fibonacci Sequence Generator

@details This lab was created for Mechatronics - ME-305-04 - Lab 01. The code contains one function which is an algorithm tasked with finding any index of the fibonacci sequence in a matter of a second.
The code is designed to run fast and effecient.
The link to the code is here: https://bitbucket.org/BENNYB0P/mechatronics/src/master/Lab%201/Fibonacci_Function.py

Created on Thu, Sep 24, 2020

Modified 9/30/2020

@author Ben Presley

@copyright Please contact the author before use of any kind.
'''


'''@section User Input Module - UI interface'''
print('\nWelcome to the Fibonacci Sequence Generator! I hope you are having a great day!')
'''@details Introduction to the program makes users happy'''

def input_user_intvar ():
    '''@section User Integer Input module
    @brief This module prompts the user to input an integer
    @details Asking the user to input for an integer can easily be punked. The user can
    easily enter a letter or a decimal. To build a robust program, we need a section of
    code that can easily deny values that will break the rest of the system. An integer
    is a common input, so let's start! Please note that the code is specific to the Fibonacci Sequence'''
    while True:
        try:
            user_index = int(input('Enter the index you would like from the sequence, n = '))
            return user_index
            '''Console input command'''
            if user_index < 0:
                '''The index must meet a list of requirements.
                If statement for negative integers.
                Recall that the index for a Fib. Seq. cannot be negative.'''
                ValueError  
                print('\nCmon, that is not a positive number, try again!')
            else:
                break
        except ValueError: 
            '''make sure it is actually an integer before exit'''
            print('\nYo, that is not an integer - try again!')
            
    
#Call the input function
user_index = input_user_intvar()

# Calculation Function
def fib_algorithm (to_index):
    '''@section Fibonacci Sequence algorithm code
    @brief Returns Fibonacci number for given index.
    @details The function recieves an integer and calculates the Fibonacci sequence up to that point.
    The algorithm is built using some great features in python -- like lists and appending to those lists.
    The first two values of the Fibonacci sequence must be defined, and the algorithm can solve for the rest.
    The sequence is effecient and can process any positve, integer index input.'''
    fiblist = [0,1] #define first two elements of the array
    if to_index == 0: #elementary case 0
        print(0)
        return 0
    else:
        if to_index == 1: #elementart case 1
            print('0\n1')
            return 1
    step = 2
    print('0\n1')
    while step <= to_index:
        fiblist.append(fiblist[-2]+fiblist[-1])
        print(fiblist[-1])
        step += 1
    return fiblist[-1]

print('\nAwesome - generating the algorithm for index, n = {:}'.format(user_index))

# Call the function
fibresults = fib_algorithm(user_index)

'''The sequence '''
print('\nThe Fibonacci algorithm has completed...')
print('\nThe value for the user index, n = {:},'.format(user_index),' is: {:}'.format(fibresults))